// REACT
import React from 'react';
import ReactDOM from 'react-dom';

// REACT Components
import FEstimate from './js/react/FEstimate';

// DB
import DB from './js/database/DB';
import { DBContext } from './js/react/hoc/DB';

// Services
import * as serviceWorker from './js/react/serviceWorker';

// REACT init
ReactDOM.render(
	<DBContext.Provider value={new DB()}>
		<FEstimate/>
	</DBContext.Provider>,
	document.getElementById('FEstimate')
);

// To make app work offline register()
serviceWorker.unregister();
