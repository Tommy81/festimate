// REACT
import React from 'react';

// DB
import withDB from './DB';

// Context
const AuthUserContext = React.createContext(null);

// HOC Components
const withAutentication = Component  => {
	class WithAuthentication extends React.Component {
		constructor(props){
			super(props);
			this.state = {
				authUser: JSON.parse(localStorage.getItem('authUser')),
			}
		}
		componentDidMount(){
			this.listener = this.props.db.onAuthUserListener(
				authUser => {
					localStorage.setItem('authUser', JSON.stringify(authUser));
					this.setState({ authUser });
				},
				() => {
					localStorage.removeItem('authUser');
					this.setState({ authUser: null });
				}
			);
		}
		componentWillUnmount(){
			this.listener();
		}
		render(){
			return(
				<AuthUserContext.Provider value={this.state.authUser}>
					<Component {...this.props} />
				</AuthUserContext.Provider>
			);
		}
	}
	return withDB(WithAuthentication);
}

// Exports
export default withAutentication;
export { AuthUserContext };
