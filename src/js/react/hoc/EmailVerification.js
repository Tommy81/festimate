// REACT
import React from 'react';

// REACT Hoc
import withDB from './DB';

// Contexts
import { AuthUserContext } from './Authentication';

// Functions
const needsEmailVerification = authUser => {
	return authUser &&
		!authUser.emailVerified &&
		authUser.providerData
			.map( provider => provider.providerId )
			.includes( 'password' );
}

// HOC
const withEmailVerification = Component => {
	class WithEmailVerification extends React.Component {
		constructor(){
			super();
			this.state = {
				isSent: false
			}
		}
		emailVerification = () => {
			this.props.db.emailVerification()
			.then(this.setState({ isSent: true }));
		}
		render(){
			const { isSent } = this.state;
			return(
				<AuthUserContext.Consumer>
					{ authUser => needsEmailVerification(authUser) ? (
						<div>
							{ isSent ? (
								<p>
									Verify your E-Mail: Check you E-Mails (Spam folder included) for a confirmation E-Mail or send another confirmation E-Mail.
								</p>
							) : (
								<p>
									Verify your E-Mail: Check you E-Mails (Spam folder included) for a confirmation E-Mail or send another confirmation E-Mail.
								</p>
							) }
							<button
								type="button"
								onClick={ this.emailVerification }>
								Invia in email il link di verifica.
							</button>
						</div>
					) : (
						<Component { ...this.props }></Component>
					) }
				</AuthUserContext.Consumer>
			);
		}
	}
	return withDB(WithEmailVerification);
}

// Exports
export default withEmailVerification;
export { needsEmailVerification };
