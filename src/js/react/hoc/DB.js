// REACT
import React from 'react';

// Context
const DBContext = React.createContext(null);

// HOC
const withDB = Component => props => {
	return (
		<DBContext.Consumer>
			{db => <Component {...props} db={db} />}
		</DBContext.Consumer>
	);
}

// Exports
export default withDB;
export { DBContext };
