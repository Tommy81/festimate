// REACT
import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

// REACT hoc components
import withDB from './DB';
import { AuthUserContext } from './Authentication';

// Routes
import { ROUTES } from '../../config';

// HOC
const withAutorization = condition => Component => {
	class WithAutorization extends React.Component {
		componentDidMount(){
			this.listener = this.props.db.onAuthUserListener(
				authUser => {
					if (!condition(authUser)) this.props.history.push(ROUTES.SIGN_IN);
				},
				() => this.props.history.push(ROUTES.SIGN_IN),
			);
		}
		componentWillUnmount(){
			this.listener();
		}
		render(){
			return(
				<AuthUserContext.Consumer>
					{ authUser => condition(authUser) ? <Component {...this.props} /> : null }
				</AuthUserContext.Consumer>
			);
		}
	}
	return compose(
		withRouter,
		withDB
	)(WithAutorization);
};

// Exports
export default withAutorization;
