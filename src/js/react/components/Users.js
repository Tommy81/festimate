// REACT
import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { compose } from 'recompose';

// REACT Hoc
import withDB from '../hoc/DB';

// Configs
import { ROUTES } from '../../config';

// Components
class UserList extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			loading: false,
			users: [],
		}
	}
	componentDidMount(){
		this.setState({ loading: true });
		this.usersRef = this.props.db.users();
		this.usersRef.on('value', s => {
			const usersObject = s.val();
			const usersList = Object.keys(usersObject).map(key => ({
				...usersObject[key],
				uid: key,
			}));
			this.setState({
				loading: false,
				users: usersList,
			});
		});
	}
	componentWillUnmount(){
		this.usersRef.off();
	}
	render(){
		const { loading, users } = this.state;
		return(
			<div>
				<h2>Users</h2>
				{ loading && <p>Loading...</p>}
				<ul>
					{users.map(user => (
						<li key={user.uid}>
							<p>
								<strong>ID:</strong> {user.uid}
							</p>
							<p>
								<Link to={{
										pathname: `${ROUTES.ADMIN}users/${user.uid}`,
										state: user,
									}}>
									Dettagli
								</Link>
							</p>
						</li>
					))}
				</ul>
			</div>
		);
	}
}
class UserItemBase extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			loading: false,
			user: props.location.state,
		}
	}
	componentDidMount(){
		if ( this.state.user ) return;
		this.setState({ loading: true });
		this.props.db
			.user(this.props.match.params.id)
			.on('value', s => {
				this.setState({
					loading: false,
					user: s.val()
				});
			})
	}
	componentWillUnmount(){
		this.props.db
			.user(this.props.match.params.id)
			.off()
	}
	resetPWD = () => {
		this.props.db.resetPwd(this.state.user.email);
	}
	render(){
		const { loading, user } = this.state;
		return(
			<div>
				<h2>User: { this.props.match.params.id }</h2>
				{ loading && <p>Loading...</p>}
				<p>
					Email: { user.email }<br></br>
					Username: { user.username }
				</p>
				<p>
					<button
						type="button"
						onClick={ this.resetPwd }
						>Reimposta la password
					</button>
					<button
						type="button"
						onClick={ this.props.history.goBack }
						>Indietro
					</button>
				</p>
			</div>
		);
	}
}
const UserItem = compose(
	withDB,
	withRouter,
)(UserItemBase);

// Exports
export default withDB(UserList);
export { UserItem };
