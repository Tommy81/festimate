// REACT
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';

// DB
import withDB from '../hoc/DB';

// ROUTES
import { ERRORS, ROUTES } from '../../config';

// Config
const INITIAL_STATE = {
	email: '',
	error: null,
	isAdmin: false,
	passwordOne: '',
	passwordTwo: '',
	username: '',
};

// Components
const SignUp = () => (
	<div>
		<h1>SignUp</h1>
		<SignUpForm />
	</div>
);
class SignUpFormBase extends React.Component {
	constructor(props) {
		super(props);
		this.state = { ...INITIAL_STATE };
	}
	onSubmit = event => {
		const { email, isAdmin, passwordOne, username } = this.state;
		event.preventDefault();
		this.props.db
			.createUser( email, isAdmin, passwordOne, username )
			.then(() => this.props.db.emailVerification())
			.then(() => {
				this.setState({ ...INITIAL_STATE });
				this.props.history.push(ROUTES.ROOT);
			})
			.catch(error => {
				if (error.code === ERRORS.ERROR_CODE_ACCOUNT_EXISTS) {
					error.message = ERRORS.ERROR_MSG_ACCOUNT_EXISTS;
				}
				this.setState({ error });
			});
	}
	onChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};
	onChangeCheckbox = event => {
		this.setState({ [event.target.name]: event.target.checked });
	}
	render() {
		const {
			email,
			error,
			isAdmin,
			passwordOne,
			passwordTwo,
			username,
		} = this.state;
		const isInvalid = passwordOne !== passwordTwo || passwordOne === '' || email === '' || username === '';
		return (
			<form onSubmit={this.onSubmit}>
				<p>
					<input
						name="username"
						value={username}
						onChange={this.onChange}
						type="text"
						placeholder="Full Name"
					/>
				</p>
				<p>
					<input
						name="email"
						value={email}
						onChange={this.onChange}
						type="text"
						placeholder="Email Address"
					/>
				</p>
				<p>
					<input
						name="passwordOne"
						value={passwordOne}
						onChange={this.onChange}
						type="password"
						placeholder="Password"
					/>
					<br></br>
					<input
						name="passwordTwo"
						value={passwordTwo}
						onChange={this.onChange}
						type="password"
						placeholder="Confirm Password"
					/>
				</p>
				<p>
					<label htmlFor="isAdmin">
						Amministratore?
						<input
							checked={isAdmin}
							name="isAdmin"
							onChange={this.onChangeCheckbox}
							type="checkbox"
						/>
					</label>
				</p>
				<p>
					<button
						type="submit"
						disabled={isInvalid}>
						Sign Up
					</button>
					<br></br>
					{error && <p>{error.message}</p>}
				</p>
			</form>
		);
	}
}
const SignUpLink = () => (
	<p>
	  Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
	</p>
);
const SignUpForm = compose(
  withRouter,
  withDB,
)(SignUpFormBase);

// Esports
export default SignUp;
export { SignUpForm, SignUpLink };
