// REACT
import React from 'react';

// DB
import withDB from '../hoc/DB';

// Components
const SignOut = ({ db }) => (
	<button onClick={db.signOut}>Logout</button>
);

// Exports
export default withDB(SignOut);
