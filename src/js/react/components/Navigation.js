// REACT
import React from 'react';
import { Link } from 'react-router-dom';

// REACT Components
import SignOut from './SignOut';

// DB
import { AuthUserContext } from '../hoc/Authentication';

// Routes
import {ROUTES} from '../../config';

// Components
const NavItem = (props) => {
	const label = props.label != null ? props.label : '';
	const mod = props.mod != null ? 'nav__item--'+props.mod : '';
	const link = props.to != null ? true : false;
	return (
		<li className={'nav__item '+mod}>
			{link && <Link {...props}>{label}</Link>}
			{props.children}
		</li>
	);
}
const Nav = (props) => {
	return(
		<nav className="nav">
			<ul className="nav_wrapper">
				{props.children}
			</ul>
		</nav>
	);
}
const NavigationAuth = () => (
	<Nav>
		<NavItem mod="projects" to={ROUTES.ROOT} label="Progetti" />
		<NavItem mod="account" to={ROUTES.ACCOUNT} label="Account" />
		<NavItem mod="account" to={ROUTES.ADMIN} label="Admin" />
		<NavItem mod="signout"><SignOut /></NavItem>
	</Nav>
);
const NavigationNoAuth = () => (
	<Nav>
		<NavItem mod="signin" to={ROUTES.SIGN_IN} label="Accedi" />
		<NavItem mod="signup" to={ROUTES.SIGN_UP} label="Registrati" />
	</Nav>
);
const Navigation = ({ authUser }) => (
	<AuthUserContext.Consumer>
		{authUser => authUser ? <NavigationAuth /> : <NavigationNoAuth /> }
	</AuthUserContext.Consumer>
);

// Exports
export default Navigation;
