// REACT
import React from 'react';
import { compose } from 'recompose';

// REACT Components
import LoginManagement from './LoginManagement';
import { PasswordForgetForm } from './PasswordForget';
import PasswordChangeForm from './PasswordChange';

// REACT hoc components
import { AuthUserContext } from '../hoc/Authentication';
import withAuthorization from '../hoc/Authorization';
import withEmailVerification from '../hoc/EmailVerification';
import withDB from '../hoc/DB';

// Config
const condition = authUser => !!authUser;

// Components
const Account = () => {
	return(
		<AuthUserContext.Consumer>
			{ authUser => (
				<div>
					<h1>Account: {authUser.email}</h1>
					<PasswordForgetForm />
					<PasswordChangeForm />
					<LoginManagement authUser={ authUser }/>
				</div>
			)}
		</AuthUserContext.Consumer>
	);
};

// Exports
export default compose(
	withEmailVerification,
	withAuthorization(condition),
	withDB
)(Account)
