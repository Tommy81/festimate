// REACT
import React from 'react';
import { compose } from 'recompose';

// REACT Components
import Button from '../components/Button';
import ProjectsList from '../components/ProjectsList';
import TextInput from '../components/TextInput';

// REACT hoc components
import withAuthorzation from '../hoc/Authorization';
import withEmailVerification from '../hoc/EmailVerification';
import { AuthUserContext } from '../hoc/Authentication';

// Config
const condition = authUser => !!authUser;

// Components
class Projects extends React.Component {
	render(){
		return (
			<AuthUserContext.Consumer>
				{ authUser => (
					authUser != null &&
					<section className="projects">
						<div className="projects__wrapper">
							<div className="projects__control">
								<TextInput />
								<Button />
							</div>
							<ProjectsList />
						</div>
					</section>
				)}
			</AuthUserContext.Consumer>
		);
	}
}

// Exports
export default compose(
	withAuthorzation(condition),
	withEmailVerification,
)(Projects);
