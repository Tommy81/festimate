// REACT
import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

// REACT components
import { PasswordForgetLink } from './PasswordForget';
import { SignUpLink } from './SignUp';

// REACT hoc
import withDB from '../hoc/DB';

// Configs
import { ERRORS, ROUTES } from '../../config';

// Config
const INITIAL_STATE = {
	email: '',
	password: '',
	error: null,
};

// Components
const SignIn = () => (
	<div>
		<h1>SignIn</h1>
		<SignInForm />
		<SignInFacebook />
		<SignInGoogle />
		<PasswordForgetLink />
		<SignUpLink />
	</div>
);
class SignInFormBase extends React.Component {
	constructor(props) {
		super(props);
		this.state = { ...INITIAL_STATE };
	}
	onSubmit = event => {
		const { email, password } = this.state;
		this.props.db
			.signIn(email, password)
			.then(() => {
				this.setState({ ...INITIAL_STATE });
				this.props.history.push(ROUTES.ROOT);
			})
			.catch(error => {
				this.setState({ error });
			});
		event.preventDefault();
	};
	onChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};
	render() {
		const { email, password, error } = this.state;
		const isInvalid = password === '' || email === '';
		return (
			<form onSubmit={this.onSubmit}>
				<input
					name="email"
					value={email}
					onChange={this.onChange}
					type="text"
					placeholder="Email Address"
				/>
				<input
					name="password"
					value={password}
					onChange={this.onChange}
					type="password"
					placeholder="Password"
				/>
				<button disabled={isInvalid} type="submit">Sign In</button>
				{error && <p>{error.message}</p>}
			</form>
		);
	}
}
class SignInFacebookBase extends React.Component {
	constructor(props) {
		super(props);
		this.state = { error: null };
	}
	onSubmit = event => {
		this.props.db
			.signInFacebook()
			.then(socialAuthUser => {
				this.setState({ error: null });
				return this.props.db
					.user(socialAuthUser.user.uid)
					.set({
						username: socialAuthUser.additionalUserInfo.profile.name,
						email: socialAuthUser.additionalUserInfo.profile.email,
						roles: {},
					});
			})
			.then(socialAuthUser => {
				this.props.history.push(ROUTES.ROOT);
			})
			.catch(error => {
				this.setState({ error });
			});
		event.preventDefault();
	};
	render() {
		const { error } = this.state;
		return (
			<form onSubmit={this.onSubmit}>
				<button type="submit">Sign In with Facebook</button>
				{error && <p>{error.message}</p>}
			</form>
		);
	}
}
class SignInGoogleBase extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			error: null
		}
	}
	onSubmit = event => {
		this.props.db.signInGoogle()
			.then( socialAuthUser => {
				return this.props.db
					.user(socialAuthUser.user.uid)
					.set({
						username: socialAuthUser.user.displayName,
						email: socialAuthUser.user.email,
						roles: {}
					});
			})
			.then(socialAuthUser => {
				this.setState({ error: null });
				this.props.history.push(ROUTES.ROOT);
			})
			.catch( error => {
				if (error.code === ERRORS.ERROR_CODE_ACCOUNT_EXISTS) {
					error.message = ERRORS.ERROR_MSG_ACCOUNT_EXISTS;
				}
				this.setState({ error });
			});
		event.preventDefault();
	}
	render(){
		const error = this.state.error;
		return(
			<form onSubmit={this.onSubmit}>
				<button type="submit">Sign in with Google</button>
				{ error && <p>{error.message}</p>}
			</form>
		);
	}
}

// Composed components
const SignInForm = compose(
	withRouter,
	withDB,
)(SignInFormBase);
const SignInFacebook = compose(
	withRouter,
	withDB,
)(SignInFacebookBase);
const SignInGoogle = compose(
	withRouter,
	withDB,
)(SignInGoogleBase);

// Exports
export default SignIn;
export { SignInForm, SignInFacebook, SignInGoogle };
