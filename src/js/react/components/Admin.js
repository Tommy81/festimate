// REACT
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { compose } from 'recompose';

// REACT Components
import UserList, { UserItem } from './Users';

// REACT hoc components
import withAuthorization from '../hoc/Authorization';
import withEmailVerification from '../hoc/EmailVerification';

// CONFIGS
import { ROLES, ROUTES } from '../../config';

// Local logic
const condition = authUser => authUser && authUser.roles[ROLES.ADMIN] != null;

// Components
const Admin = () => {
	return (
		<div>
			<h1>Admin</h1>
			<p>The Admin Page is accessible by every signed in admin user.</p>
			<Switch>
				<Route exact path={ ROUTES.ADMIN_DETAILS } component={ UserItem } />
				<Route exact path={ ROUTES.ADMIN } component={ UserList } />
			</Switch>
		</div>
	);
}

export default compose(
	withAuthorization(condition),
	withEmailVerification,
)(Admin);
