// REACT
import React from 'react';

// REACT Hoc
import withDB from '../hoc/DB';

// Configs
import { SIGN_IN_METHODS } from '../../config';

// React Components
class LoginManagement extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			activeSignMethods: [],
			error: null,
		}
	}
	componentDidMount(){
		this.fetchSignInMethods();
	}
	fetchSignInMethods = () => {
		this.props.db
			.fetchSignInMethods(this.props.authUser.email)
			.then( activeSignMethods =>
				this.setState({ activeSignMethods , error: null }))
			.catch( error => this.setState({ error }));
	}
	onDefaultLink = password => {
		this.props.db.linkDefaultLogin(
			this.props.authUser.email,
			password)
			.then(this.fetchSignInMethods)
			.catch( error => this.setState({ error }));
	}
	onSocialLoginLink = provider => {
		this.props.db
			.linkSocialLogin(provider)
			.then(this.fetchSignInMethods)
			.catch( error => {this.setState({ error });});
	}
	onUnLink = providerId => {
		this.props.db
			.unLinkDefaultLogin(providerId)
			.then(this.fetchSignInMethods)
			.catch( error => this.setState({ error }));
	}
	render(){
		const { activeSignMethods, error } = this.state;
		return(
			<div>
				<h2>Gestione modalità di autenticazione</h2>
				<ul>
					{ SIGN_IN_METHODS.map( signInMethod => {
						const onlyOneLeft = activeSignMethods.length === 1;
						const isEnabled = activeSignMethods.includes(signInMethod.id);
						return (
							<li key={ signInMethod.id }>
								{ signInMethod.id === "password" ? (
									<DefaultLoginToggle
										isEnabled={ isEnabled }
										onlyOneLeft={ onlyOneLeft }
										onLink={ this.onDefaultLink }
										onUnLink={ this.onUnLink }
										signInMethod={ signInMethod }
									/>
								) : (
									<SocialLoginToggle
										isEnabled={ isEnabled }
										onlyOneLeft={ onlyOneLeft }
										onLink={ this.onSocialLoginLink }
										onUnLink={ this.onUnLink }
										signInMethod={ signInMethod }
									/>
								)}
							</li>
						);
					}) }
				</ul>
				{ error && <p>{ error.message }</p>}
			</div>
		);
	}
}
class DefaultLoginToggle extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			password: ''
		}
	}
	onChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	}
	onSubmit = event => {
		event.preventDefault();
		this.props.onLink( this.state.password );
		this.setState({ password: '' });
	}
	render(){
		const {
			isEnabled,
			onlyOneLeft,
			onUnLink,
			signInMethod,
		} = this.props;
		const { password } = this.state;
		const isInvalid = password === '';
		return isEnabled  ? (
				<button
					disabled={ onlyOneLeft }
					type="button"
					onClick={ onUnlink => {
						onUnLink( signInMethod.id )
					} } >
					Disattiva { signInMethod.id }
				</button>
			) : (
				<form onSubmit={ this.onSubmit }>
					<input
						name="password"
						onChange={ this.onChange }
						placeholder="password"
						type="password"
						value={ password }
					/>
					<button
						disabled={ isInvalid }
						type="submit">
						Attiva { signInMethod.id }
					</button>
				</form>
			);
	}
}
const SocialLoginToggle = ({
		isEnabled,
		onlyOneLeft,
		onLink,
		onUnLink,
		signInMethod
	}) => {
		return(isEnabled ? (
			<button
				disabled={ onlyOneLeft }
				type="button"
				onClick={ () => onUnLink(signInMethod.id) }>
				Disattiva { signInMethod.id }
			</button>
		) : (
			<button
				type="button"
				onClick={ () => onLink(signInMethod.provider) }>
				Attiva { signInMethod.id }
			</button>
		));
	}

// Exports
export default withDB(LoginManagement);
