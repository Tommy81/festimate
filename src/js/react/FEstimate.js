// REACT
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// REACT Components
import Account from './components/Account';
import Admin from './components/Admin';
import Navigation from './components/Navigation';
import PasswordForget from './components/PasswordForget';
import Projects from './components/Projects';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';

// REACT hoc components
import withAutentication from './hoc/Authentication';

// Routes
import {ROUTES} from '../config';

// The Class
class FEstimate extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			phase: null
		}
	}
	setPhase = nextPhase => {
		this.setState(()=>{
			return { phase: nextPhase };
		});
	}
	render(){
		return (
			<div className="Festimate">
				<Router>
					<Navigation setPhase={this.setPhase} />
					<Route exact path={ROUTES.ROOT} component={Projects}></Route>
					<Route path={ROUTES.ACCOUNT} component={Account}></Route>
					<Route path={ROUTES.ADMIN} component={Admin}></Route>
					<Route path={ROUTES.PASSWORD_FORGET} component={PasswordForget}></Route>
					<Route path={ROUTES.SIGN_IN} component={SignIn}></Route>
					<Route path={ROUTES.SIGN_UP} component={SignUp}></Route>
				</Router>
			</div>
		);
	}
}

// Exports
export default withAutentication(FEstimate);
