// FIREBASE
import * as firebase from "firebase/app";
import 'firebase/auth';
import 'firebase/database';
import firebaseConfig from './firebase.js';

// DB
import { DBREFS, ROLES, URLS } from '../config';

// The Class
class DB {
	constructor(){

		// Firebase app initialization
		firebase.initializeApp(firebaseConfig);

		// Firebase APIs
		this.auth = firebase.auth();
		this.db = firebase.database();

		// SignIn providers
		this.emailAuthProvider = firebase.auth.EmailAuthProvider;
		this.facebookProvider = new firebase.auth.FacebookAuthProvider();
		this.googleProvider = new firebase.auth.GoogleAuthProvider();

		// Properties
		this.ref = DBREFS;

	}

	// Users, Accounts management, Logins
	createUser = ( email, isAdmin, password, username ) => {
		return this.auth.createUserWithEmailAndPassword( email, password )
			.then(authUser => {
				const roles = {};
				if (isAdmin) roles[ROLES.ADMIN] = ROLES.ADMIN;
				this.user(authUser.user.uid)
					.set({
						email,
						roles,
						username
					});
			});
	}
	emailVerification = () => {
		return this.auth.currentUser
			.sendEmailVerification({
				url: URLS.EMAIL_VERIFICATION_REDIRECT
			});
	}
	getUsername = (uid) => {
		return this.user(uid).once('value').then(s => s.val().username);
	}
	fetchSignInMethods = email => {
		return this.auth
			.fetchSignInMethodsForEmail(email);
	}
	linkDefaultLogin = ( email, password ) => {
		const credential = this.emailAuthProvider.credential(
			email,
			password
		);
		return this.auth.currentUser
			.linkWithCredential(credential);
	}
	linkSocialLogin = provider => {
		const user = this.auth.currentUser;
		return user.linkWithPopup(this[provider]);
	}
	onAuthUserListener = (next, fallback) => {
		return this.auth
			.onAuthStateChanged(authUser => {
				if(authUser){
					this.user(authUser.uid)
						.once('value')
						.then( s => {
							const dbUser = s.val();
							if (!dbUser.roles) dbUser.roles = {};
							authUser = {
								uid: authUser.uid,
								email: authUser.email,
								emailVerified: authUser.emailVerified,
								providerData: authUser.providerData,
								...dbUser,
							}
							next(authUser);
						});
				}
				else fallback();
			});
	}
	resetPwd = email => this.auth.sendPasswordResetEmail(email);
	signIn = (email,pwd) => this.auth.signInWithEmailAndPassword(email,pwd);
	signInFacebook = () => this.auth.signInWithPopup(this.facebookProvider);
	signInGoogle = () => this.auth.signInWithPopup(this.googleProvider);
	signOut = () => this.auth.signOut();
	updatePwd = pwd => this.auth.updatePassword(pwd);
	unLinkDefaultLogin = providerId => {
		const user = this.auth.currentUser;
		return user.unlink(providerId);
	}
	user = uid => this.db.ref(`users/${uid}`);
	users = () => this.db.ref('users/');

}

// Exports
export default DB;
