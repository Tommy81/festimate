// DATABASE REFS
const DBREFS = {
	PROJECTS: '/projects/',
}

// ERRORS
const ERRORS = {
	ERROR_CODE_ACCOUNT_EXISTS:
		'auth/account-exists-with-different-credential',
	ERROR_MSG_ACCOUNT_EXISTS:
		`An account with an E-Mail address to
		this social account already exists. Try to login from
		this account instead and associate your social accounts on
		your personal account page.`
}

// SIGNI IN METHODS
const SIGN_IN_METHODS = [
	{
		id: 'password',
		provider: 'emailAuthProvider',
	},
	{
		id: 'google.com',
		provider: 'googleProvider',
	},
	{
		id: 'facebook.com',
		provider: 'facebookProvider',
	}
];

// ROUTES
const ROUTES = {
	ACCOUNT: '/account/',
	ADMIN: '/admin/',
	ADMIN_DETAILS: '/admin/users/:id',
	ROOT: '/',
	PASSWORD_FORGET: '/passwordforget/',
	SIGN_IN: '/signin/',
	SIGN_UP: '/signup/',
}

// ROLES
const ROLES = {
	ADMIN: 'ADMIN',
}

// URLS
const URLS = {
	EMAIL_VERIFICATION_REDIRECT: 'http://localhost:3000',
}

// Exports
export {
	DBREFS,
	ERRORS,
	ROLES,
	ROUTES,
	SIGN_IN_METHODS,
	URLS,
};
